from phyloutl import create_tree_from_seqs, parse_path, clean_raxml_auxs
from shared import *
from time import sleep
from tqdm import tqdm
import multiprocessing as mp
import argparse
import pickle 


def build(genes, n, cmd='raxmlHPC', start='starting_tree.newick'):
    for gene in tqdm(genes):
        gene = gene.upper()
        fastas_path = '{}/{}.fasta'.format(alignments_folder, gene)
        out_path = '{}/{}.newick'.format(phylotrees_folder, gene)
        out_path_img = '{}/{}.png'.format(phylotrees_folder, gene)
        parse_path(out_path)
        t = create_tree_from_seqs(fastas_path, out_path, n=n,
                                  outgroup=phylo_outgroup, cmd=cmd,
                                  start=start)


def parse_argv():
    parser = argparse.ArgumentParser(description="Generate random models.")
    parser.add_argument("--n", type=int, default=5)
    parser.add_argument("--num_cores", type=int, default=-1)
    parser.add_argument('--cmd', type=str, default='raxmlHPC')
    parser.add_argument('--start', type=str, default='starting_tree.newick')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_argv()
    procs = list()
    
    clean_raxml_auxs()
    with open(filename_genes_clean, 'rb') as f:
        genes = pickle.load(f)
        genes = list(genes.keys())
    n_cores = args.num_cores
    if n_cores <= 0:
        n_cores = mp.cpu_count()
    if n_cores >= len(genes):
        n_cores = len(genes)
        sz = 1
    else:
        sz = len(genes) // n_cores + 1
    
    for i in range(n_cores):
        arg = genes[i * sz: (i + 1) * sz]
        procs.append(mp.Process(target=build, args=(arg, args.n, args.cmd,
                                                    args.start)))
        procs[-1].start()
    
    waitflag = False
    while not waitflag:
        sleep(1)
        waitflag = True
        for proc in procs:
            if proc.is_alive():
                waitflag= False
                break