from shared import *
from phyloutl import parse_path, multiple_alignment
from tqdm import tqdm
from time import sleep
import multiprocessing as mp
import pickle

def msa(genes):
    for gene in tqdm(genes):
        gene = gene.upper()
        fastas_path = '{}/{}.fasta'.format(seq_folder, gene)
        out_path = '{}/{}.fasta'.format(alignments_folder, gene)
        parse_path(out_path)
        multiple_alignment(fastas_path, out_path)

n_cores = mp.cpu_count()
with open(filename_genes_clean, 'rb') as f:
    genes = pickle.load(f)
    genes = list(genes.keys())

if n_cores >= len(genes):
    n_cores = len(genes)
    sz = 1
else:
    sz = len(genes) // n_cores + 1
procs = list()
for i in range(n_cores):
    arg = genes[i * sz: (i + 1) * sz]
    procs.append(mp.Process(target=msa, args=(arg,)))
    procs[-1].start()

waitflag = False
while not waitflag:
    sleep(1)
    waitflag = True
    for proc in procs:
        if proc.is_alive():
            waitflag= False
            break