from phyloutl import parse_path, retry
from collections import defaultdict
from ftplib import FTP
from Bio import SeqIO
import biomart as bm
import pandas as pd
import shutil
import gzip
import os


__FTP_SERVER = 'ftp.ensembl.org'
__SPECIES_DIR = 'pub/release-96/fasta'
__DESC_GENE_P = 'gene_symbol:'
__DESC_GENE2_P = ' gene:'
__DESC_PATTERN = __DESC_GENE_P + '{}'
__MAX_ATTRIBUTES = 6



def connect():
    global __FTP_SERVER
    global __SPECIES_DIR
    ftp = FTP(__FTP_SERVER)
    ftp.login()
    ftp.cwd(__SPECIES_DIR)
    return ftp

def download_genome(specie: str, directory: str):
    """Retrieves specie's genome.
    Keyword arguments:
        specie    -- A specie's name.
        directory -- A name of directory where FASTA file will be stored.
    Returns:
        A path to FASTA file containing specie's reference genome.
    """
    specie = specie.lower().replace(' ', '_')
    path = '{}/{}'.format(directory, specie)
    path_fasta = path + '.fasta'
    if os.path.isfile('{}/{}.fasta'.format(directory, specie)):
        return path_fasta
    ftp = connect()
    ftp.cwd(specie + '/cds')
    file = next(f for f in ftp.nlst() if '.cds.' in f and f.endswith('.gz'))
    parse_path(path)
    path_gz = path + '.gz'
    with open(path_gz, 'wb') as f:
        ftp.retrbinary('RETR {}'.format(file), f.write)
    ftp.quit()
    with gzip.open(path_gz, 'rb') as f_in:
        with open(path_fasta, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
    os.remove(path_gz)
    return path_fasta

def get_gene(fasta: str, gene: str, directory: str, new_id=None):
    """Retrieves a specific gene from FASTA file and saves it as a separate
    FASTA file to a directory.
    Keyword arguments:
        fasta     -- A path to FASTA file containining at least one entry.
        gene      -- A name of gene.
        directory -- A path to output directory where gene in FASTA format is
                     to be stored.
        new_id    -- An FASTA id to be used.
    """
    global __DESC_PATTERN
    p = __DESC_PATTERN.format(gene).lower()
    path = '{}/{}.fasta'.format(directory, gene)
    if os.path.isfile(path):
        return
    try:
        rec = next(record for record in SeqIO.parse(fasta, 'fasta')
                   if p in record.description.lower())
    except StopIteration:
        raise Exception("No gene {} in {}.".format(gene, fasta))
    if new_id:
        seq = SeqIO.SeqRecord(rec.seq, id=new_id)
    else:
        seq = rec
    parse_path(path)
    SeqIO.write(seq, path, 'fasta')

def get_genes(fasta: str, genes, specie: str, directory: str, new_id=None):
    """Retrieves specific genes from FASTA file and saves it as a separate
    FASTA file to a directory.
    Keyword arguments:
        fasta     -- A path to FASTA file containining at least one entry.
        genes     -- Genes' names dictionary as returned by get_genes_names.
        specie    -- A specie's name.
        directory -- A path to output directory where gene in FASTA format is
                     to be stored.
        new_id    -- An FASTA id to be used.
    Returns:
        List of genes that were not found.
    """
    global __DESC_GENE2_P
    plen = len(__DESC_GENE2_P)
    t = list()
    for gene in genes:
        path = '{}/{}.fasta'.format(directory, gene)
        if not os.path.isfile(path):
            t.append(gene)
    parse_path(path) # A dumb trick to crate directories.
    genes_names = t
    for record in SeqIO.parse(fasta, 'fasta'):
        i = record.description.find(__DESC_GENE2_P)
        if i < 0:
            continue
        desc = record.description[i + plen:].lower()
        for gene in reversed(genes_names):
            flag = False
            for synonym in genes[gene][specie]:
                    if desc.startswith(synonym.lower()):
                        flag = True
                        break
            if not flag:
                continue
            path = '{}/{}.fasta'.format(directory, gene.upper())
            genes_names.remove(gene)
            if new_id:
                seq = SeqIO.SeqRecord(record.seq, id=new_id)
            else:
                seq = record
            SeqIO.write(seq, path, 'fasta')
        if not genes:
            break
    return list(map(lambda x: x.upper(), genes_names))
    
#def get_genes_names(fasta: str):
#    """Retrieves names of all genes present in FASTA file.
#    Keyword arguments:
#        fasta -- A path to FASTA file.
#    Returns:
#        A set of genes.
#    """
#    global __DESC_GENE_P
#    plen = len(__DESC_GENE_P)
#    genes = set()
#    for record in SeqIO.parse(fasta, 'fasta'):
#        i = record.description.find(__DESC_GENE_P)
#        if i < 0:
#            continue
#        desc = record.description[i + plen:]
#        i = desc.find(' ')
#        desc = desc[:i]
#        genes.add(desc.upper())
#    return genes

def parse_gmt(filename: str):
    with open(filename, 'r') as f:
        lines = f.readlines()
    genes = set()
    for line in lines:
        genes.update(map(lambda t: t.strip().upper(), line.split('\t')[2:]))
    return genes
        

def picklable_dd():
    return defaultdict(list)
    
def test_species(species: list):
    pass
    
def get_genes_names(species: list):
    global __MAX_ATTRIBUTES
    spcs = [spc.split(' ') for spc in species]
    spcs = [spc[0].lower()[0] + spc[1] for spc in spcs]
    ds_name = '{}_gene_ensembl'.format(spcs[0])
    spcs = spcs[1:]
    filters = {'with_{}_homolog'.format(spc): True for spc in spcs}
    filters['with_ccds'] = True
    attributes = ['ensembl_gene_id'] +['{}_homolog_ensembl_gene'.format(spc)
                                       for spc in spcs]
    server = retry(bm.BiomartServer, 'http://www.ensembl.org/biomart')
    ds = server.datasets[ds_name]
#    response = ds.search(params={'filters': filters,
#                                 'attributes': ['hgnc_symbol']})
#    genes = [[gene] for gene in response.text.split('\n')]
#    print(len(genes))
#    attributes = [attrs[i:i + __MAX_ATTRIBUTES - 1]
#                  for i in range(0, len(attrs), __MAX_ATTRIBUTES - 1)]
    genes = defaultdict(picklable_dd)
    for i in range(0, len(attributes), __MAX_ATTRIBUTES - 1):
        attrs = ['external_gene_name'] + attributes[i:i + __MAX_ATTRIBUTES - 1]
        response = retry(ds.search, params={'filters': filters, 
                                     'attributes': attrs})
        subspecies = species[i:]
        for line in response.text.split('\n'):
            gs = line.split('\t')
            for specie, gene in zip(subspecies, gs[1:]):
                if gene:
                    genes[gs[0]][specie].append(gene)
    removable = set()
    set_species = set(species[1:])
    for gene, spcs in genes.items():
        if not set_species.issubset(set(spcs)):
            removable.add(gene)
    for gene in removable:
        del genes[gene]
    return genes