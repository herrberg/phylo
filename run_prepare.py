from phyloutl import parse_path
from genome import download_genome, get_genes, get_genes_names
from collections import defaultdict
from shared import *
import pickle
import os

 
with open(filename_species, 'r') as f:
    species = [line for line in f.read().split('\n')
               if line.lstrip()[0] != '#']
    species = list(filter(lambda t: t, map(lambda t: t.strip(), species)))

if os.path.isfile(filename_genes):
    with open(filename_genes, 'rb') as f:
        genes = pickle.load(f)
else:
    print("Composing a list of genes...")
    genes = get_genes_names(species)
    with open(filename_genes, 'wb') as f:
        pickle.dump(genes, f)

    
def prepare_genes():
    """Downloads reference genomes if necessary and retrieves genes from them.
    """
    global genes
    global species
    global genome_folder
    genes_not_found = dict()
    print("Retrieving genome for all species.")
    print("It will take some time if ran for the first time.")
    for specie in species:
        print("Retrieving reference genome for specie {}...".format(specie))
        genome = download_genome(specie, genome_folder)
        path = '{}/{}'.format(genome_folder, specie)
        missed_genes = get_genes(genome, genes, specie, path)
        if missed_genes:
            genes_not_found[specie] = missed_genes
            missed_genes = ', '.join(missed_genes)
            print('{}: failed to retrieve genes {}'.format(specie, missed_genes))
    return genes_not_found

def get_faulty_genes(start_codon=True, stop_codon=True, triplets=True):
    global genes
    global species
    global genome_folder
    fault_genes = defaultdict(list)
    specie_counter = defaultdict(int)
    for specie in species:
        print('Checking for fault genes for {}'.format(specie))
        for gene in genes:
            with open('{}/{}/{}.fasta'.format(genome_folder, specie, gene.upper()), 'r') as f:
                fasta = f.readlines()[1:]
            fasta = ''.join(fasta).strip().replace('\n', '')
            if start_codon and fasta[:3] != 'ATG':
                fault_genes[gene].append(specie)
                specie_counter[specie] += 1
            elif stop_codon and fasta[-3:] not in ('TAA', 'TGA', 'TAG'):
                fault_genes[gene].append(specie)
                specie_counter[specie] += 1                
            elif triplets and len(fasta) % 3 != 0:
                fault_genes[gene].append(specie)
                specie_counter[specie] += 1
    for specie in species:
        specie_counter[specie] /= len(genes)
    return fault_genes, specie_counter

missed_genes = prepare_genes()

if os.path.isfile(filename_genes_clean):
    with open(filename_genes_clean, 'rb') as f:
        genes = pickle.load(f)
else:
    fault_genes, fault_genes_freq = get_faulty_genes(start_codon=False,
                                                     stop_codon=False)
    for gene in fault_genes:
        del genes[gene]
    with open(filename_genes_clean, 'wb') as f:
        pickle.dump(genes, f)

# Alignment
print("Compiling genes to multiple-fasta files...")
for gene in genes:
    gene = gene.upper()
    ptrn = '{}/{{}}/{}.fasta'.format(genome_folder, gene)
    fastas_path = '{}/{}.fasta'.format(seq_folder, gene)
    if not os.path.isfile(fastas_path):
        parse_path(fastas_path)
        fastas = [ptrn.format(specie) for specie in species]
        flist = list()
        for fasta in fastas:
            spc = fasta.split('/')[1].replace(' ', '_')
#            if spc not in fault_genes[gene]:
#                continue
            with open(fasta, 'r') as f:
                t = ['>{}\n'.format(spc)] + f.readlines()[1:]
                flist.append(''.join(t))
        with open(fastas_path, 'w') as f:
            f.write(''.join(flist))        
    
#import matplotlib.pyplot as plt
#from itertools import combinations
#import upsetplot as up
#plt.figure(figsize=(36, 10))
##d = {org: len(missed_genes[org]) for org in missed_genes}}
#x = list(range(len(missed_genes_counter)))
#y = sorted(missed_genes_counter.values())
#y = y[len(y) % 2::2] + y[::-2]
##plt.bar(range(len(missed_genes_counter)), missed_genes_counter.values(), 1, edgecolor='black')
#plt.plot(x, y)
#plt.savefig('da.jpg')
#lt1 = list()
#lt2 = list()
#for i in range(2, 3):
#    print(i)
#    for ss in combinations(species, i):
#        lt1.append(ss)
#        sets = [set(missed_genes[org]) for org in ss]
#        lt2.append(len(sets[0].intersection(*sets)))
#t = up.from_memberships(lt1, lt2)
#plt.figure(figsize=(60, 30))
#up.plot(t)
#plt.savefig('up.jpg')
#cleanup_genes(genes, missed_genes_ratios)
#pm = PhyloManager(genes, species, 'Homo sapien')
#pm.retrieve_sequences(genome_folder)
#pm.alignments(genome_folder, alignments_folder)
